import javafx.event.EventHandler;
import javafx.event.ActionEvent ;
import javafx.scene.control.Button;

public class ControleurButtonCo implements EventHandler<ActionEvent> {
    private AppliAllo45 app;

    public ControleurButtonCo(AppliAllo45 app){
        this.app = app;
    }
    
    @Override
    public void handle(ActionEvent event){
        Button button = (Button) (event.getSource());
        if (button.getText().contains("Connexion")){
            this.app.afficheFenetreCo();
        }
        else{
            this.app.afficheFenetreDeco();
        }
    }
}
