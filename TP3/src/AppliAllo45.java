import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class AppliAllo45 extends Application {
    private Button btnConnexion;
    private Button btnDeco;
    private Scene scene;

    public static void main(String[] args) {
        launch(AppliAllo45.class, args);
    }

    @Override
    public void init(){
        this.btnConnexion = new Button("Connexion");
        ImageView img = new ImageView("user.png");
        Button btnDeco = new Button("Deconnexion", img);

        //le link avec controleur
        ControleurButtonCo controleur = new ControleurButtonCo(this);        
        this.btnConnexion.setOnAction(controleur);
        this.btnDeco.setOnAction(controleur);
    }
    @Override
    public void start(Stage stage) throws Exception {
        Pane root = new FenetreConnexion(this.btnConnexion);
        this.scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Appli avec deux fenêtres");
        stage.show();
        
    }
    public void afficheFenetreCo(){
        Pane root = new FenetreConnexion(this.btnConnexion);
        this.scene.setRoot(root);
    }
    public void afficheFenetreDeco(){
        Pane root = new FenetreAnalyste(this.btnDeco);
        this.scene.setRoot(root);
    }
}
