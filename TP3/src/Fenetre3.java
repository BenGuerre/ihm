import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class Fenetre3 extends VBox{
    private Button btn;
    public Fenetre3(Button btn){
        super();
        this.btn = btn;
        //L'image
        this.getChildren().add(new ImageView(new Image("https://publicdomainvectors.org/photos/Calculator-Icon.png")));
        //Button en dernier
        this.getChildren().add(this.btn);
    }
    
}
