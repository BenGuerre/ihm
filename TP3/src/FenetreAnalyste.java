
import javafx.scene.image.ImageView;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Side;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.layout.TilePane;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;

public class FenetreAnalyste extends BorderPane {
    private Button btnConnexion;

    public FenetreAnalyste(Button button){
        //le bouton
        this.btnConnexion = button;

        this.addTopPart(this);
        this.addCenterPart(this);
        this.addRightPart(this);
    }    

    public void addTopPart(BorderPane root){
        /*
        Ceéation du header
        */

        BorderPane topBorderPane = new BorderPane();
        //Le titre
        Text titre = new Text("Allo 45-Module Analyste");
        titre.setFont(Font.font("Arial", FontWeight.BOLD, 32));
        topBorderPane.setLeft(titre);
        //Le button
        topBorderPane.setRight(btnConnexion);
        //Le background
        topBorderPane.setBackground(new Background(new BackgroundFill(Color.ROYALBLUE, null, null)));
        //Le padding 
        topBorderPane.setPadding(new Insets(10));
        root.setTop(topBorderPane);
    }

    public void addCenterPart(BorderPane root){
        /*
        Créer le centre
        */
        VBox center = new VBox();
        // Crréation du titre
        Text titre = new Text("Analyse du sondage sur les habitudes alimentaires");
        titre.setFont(Font.font("Arial", FontWeight.BOLD, 22));
        //Crétion du combot box
        ComboBox<String> conbBox = new ComboBox<String>();
        conbBox.getItems().add("Pie");
        conbBox.getItems().add("Line");
        conbBox.getItems().add("Bar");
        conbBox.getItems().add("Data");
        conbBox.getItems().add("Location");
        conbBox.getItems().add("Area");
        conbBox.getItems().add("Relative");
        conbBox.getSelectionModel().select("Pie");
        //Création du cammenbert
        PieChart chart = new PieChart () ;
        chart.setTitle( " Que lisez-vous au petit déjeuner ? " ) ;
        chart.getData().setAll (
            new PieChart.Data( " Le journal " , 21) ,
            new PieChart.Data( " Un livre " , 3) ,
            new PieChart.Data( " Le courier " , 7) ,
            new PieChart.Data( " La boîte de céréales " , 75) ) ;
        chart.setLegendSide( Side.LEFT ) ; // pour mettre la l é gende à gauche
        //Creation du HBox
        HBox hBox = new HBox();
            //Creation des deux boutons du hbox
        ImageView backArrow = new ImageView("back.png");
        Button back = new Button("Question précédente", backArrow);
        ImageView nextArrow = new ImageView("next.png");
        Button next = new Button("Question suivant", nextArrow);
            //On les me dans la hbox
        hBox.getChildren().addAll(back, next);
            //Le margin pour les boutons
        hBox.setSpacing(10);
        //Les lie entre eux 
        center.getChildren().addAll(titre, conbBox, chart, hBox);
        //Le padding
        center.setPadding(new Insets(10));
        root.setCenter(center);
    }

    public void addRightPart(BorderPane root){
        TilePane thePane = new TilePane();
        //Creation des images view
        ImageView img1 = new ImageView("chart_1.png");
        ImageView img2 = new ImageView("chart_2.png");
        ImageView img3 = new ImageView("chart_3.png");
        ImageView img4 = new ImageView("chart_4.png");
        ImageView img5 = new ImageView("chart_5.png");
        ImageView img6 = new ImageView("chart_6.png");
        ImageView img7 = new ImageView("chart_7.png");
        
        //Le background
        thePane.setBackground(new Background(new BackgroundFill(Color.AZURE, null, null)));
        //lien des images pour afficher
        thePane.getChildren().addAll(img1,img2,img3,img4,img5,img6,img7);
        root.setRight(thePane);
    }
}
