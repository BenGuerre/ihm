import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Button;
import javafx.scene.control.Label;


public class FenetreConnexion extends GridPane{
    TextField textFieldId;
    TextField textFieldMdp;
    Label labelId;
    Label labelMdp;
    Button buttonConnexion;
    Label explication;
    
    public FenetreConnexion(Button button){
        //init du bouton
        super();
        this.buttonConnexion = button;
        
        //Construction de la fenetre
        this.ajouteLabelExplication(this);
        this.ajouteTextFieldId(this);
        this.ajouteTextFieldMdp(this);
        this.ajouteLabelId(this);
        this.ajouteLabelMdp(this);
        this.ajouteButton(this);
    }
        

    public void ajouteTextFieldId(GridPane root){
        this.textFieldId = new TextField();
        root.add(textFieldId, 1, 1, 2, 1);
    }

    public void ajouteTextFieldMdp(GridPane root){
        this.textFieldMdp = new TextField();
        root.add(textFieldMdp, 1, 2, 2, 1);
    }

    public void ajouteLabelId(GridPane root){
        this.labelId = new Label("Identifiant");
        root.add(labelId, 0, 1);
    }

    public void ajouteLabelMdp(GridPane root){
        this.labelMdp = new Label("Mot de passe");
        root.add(labelMdp, 0, 2);
    }

    public void ajouteLabelExplication(GridPane root){
        this.explication = new Label("Entrez votre identifaint et votre mot de passe");
        root.add(this.explication, 0, 0, 2, 1);
    }

    public void ajouteButton(GridPane root){
        root.add(this.buttonConnexion, 2, 3);
    }

}