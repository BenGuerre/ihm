import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurReset implements EventHandler<ActionEvent>{
    private AppliSomme appli;

    public ControleurReset(AppliSomme app){
        this.appli = app;
    }

    public void handle(ActionEvent e){
        System.out.println("Reset");
        appli.efface();
    }
}