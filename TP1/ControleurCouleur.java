import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.event.EventHandler;



public class ControleurCouleur implements EventHandler<MouseEvent>{
    private AppliDessin appli;
    private Color couleur;
    private double x;
    private double y;
    
    public ControleurCouleur(AppliDessin appli, Color couleur){
        this.appli = appli;
        this.couleur = couleur;
    }

    public void handle(MouseEvent event){
        this.appli.changeCouleur(this.couleur);
    }
}
