import javafx.scene.control.TextField;
import javafx.application.Application;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.layout.Pane;
import javafx.scene.Scene;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;





public class AppCalcul extends Application {
    protected TextField textField1;
    protected TextField textField2;
    protected Label resultatText;
    protected double res;
    @Override
    public void init(){
        //Initialisation des objets qui ne sont pas dans la scène 
        // cad non graphique
    }
    
    @Override
    public void start(Stage stage) throws Exception{
        //Contruction de la fenêtre
        VBox root =  new VBox();

        this.ajouteTextField(root);
        this.ajouteBouttons(root);
        this.ajouteResultat(root);

        Scene scene = new Scene(root);
        stage.setTitle("Calculatrice");
        stage.setScene(scene);
        stage.show();
    }

    public double getValueTextField1() throws NumberFormatException{
        return Double.parseDouble(textField1.getText());
    }

    public double getValueTextField2() throws NumberFormatException{
        return Double.parseDouble(textField2.getText());
    }

    public void setResultat(double res){
        this.resultatText.setText("Résultat : " + res);
    }
    public void setResultatErr(){
        this.resultatText.setText("Une des valeurs entrée n'est pas un chiffre, veuillez modifier");
    }
    public void quitte(){
        Platform.exit();
    }

    private void ajouteTextField(Pane root){
        HBox hbTextfield = new HBox();
        this.textField1 = new TextField();
        this.textField2 = new TextField();
        hbTextfield.getChildren().addAll(textField1, textField2);
        hbTextfield.setAlignment(Pos.CENTER);
        root.getChildren().add(hbTextfield);

    }


    private void ajouteBouttons(Pane root){
        HBox hbButtons = new HBox(3);
        hbButtons.setPadding(new Insets(0, 0, 10, 10));
        Button buttonPlus = new Button("+");
        Button buttonMoins = new Button("-");
        Button buttonMult = new Button("*");
        Button buttonDiv = new Button("/");
        hbButtons.getChildren().addAll(buttonMoins, buttonPlus, buttonMult, buttonDiv);
        hbButtons.setAlignment(Pos.CENTER);
        buttonPlus.setOnAction(new ControleurButtonPlus(this, "+"));
        buttonMoins.setOnAction(new ControleurButtonPlus(this, "-"));
        buttonDiv.setOnAction(new ControleurButtonPlus(this, "/"));
        buttonMult.setOnAction(new ControleurButtonPlus(this, "*"));
        root.getChildren().add(hbButtons);


    }
    private void ajouteResultat(Pane root){
        this.resultatText = new Label();
        resultatText.setText("Résultat : " +"\n ");
        resultatText.setAlignment(Pos.CENTER);
        root.getChildren().add(resultatText);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
