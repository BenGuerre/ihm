public class Calcul {
    double y;
    double x;

    public Calcul(double x, double y){
        this.x = x;
        this.y = y;
    }

    public double add(){
        return this.x + this.y;
    }

    public double sub(){
        return this.x - this.y;
    }

    public double mult(){
        return this.x * this.y;
    }

    public double div(){
        return this.x / this.y;
    }
}
