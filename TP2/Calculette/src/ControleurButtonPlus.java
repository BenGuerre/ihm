import javafx.event.EventHandler;
import javafx.event.ActionEvent;
public class ControleurButtonPlus implements EventHandler<ActionEvent> {
    private AppCalcul appli;
    private String signe;
    private double res;

    public ControleurButtonPlus(AppCalcul app, String signe){

        this.appli = app;
        this.signe = signe;
    }

    @Override
    public void handle(ActionEvent event) {
        try{
            Calcul calcul = new Calcul(appli.getValueTextField1(), appli.getValueTextField2());
            if (this.signe == "+"){
                appli.setResultat(calcul.add());
            }
            else if (this.signe == "*"){
                appli.setResultat(calcul.mult());
            }
            else if (this.signe == "/"){
                appli.setResultat(calcul.div());
            }
            else{
                appli.setResultat(calcul.sub());
            }
        }
        catch (NumberFormatException exp){
            appli.setResultatErr();
        }
    }
}
